import React, { useState, useRef, useEffect } from 'react';

const Form = (props) => {
  const [input, setInput] = useState(props.edit ? props.edit.value : '');

  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  });

  function changeHandler(event) {
    setInput(event.target.value);
  }

  const submitHandler = (event) => {
    event.preventDefault();

    props.onSubmit({
      id: 'todo-' + Math.floor(Math.random() * 100000),
      text: input,
    });

    setInput('');
  };

  return (
    <form className="todo-form" onSubmit={submitHandler}>
      {props.edit ? (
        <>
          <input
            type="text"
            name="text"
            value={input}
            id="todo"
            placeholder="Update Todo"
            className="todo-input edit"
            onChange={changeHandler}
            ref={inputRef}
          />
          <button className="todo-button edit" type="submit">
            Update Todo
          </button>
        </>
      ) : (
        <>
          <input
            type="text"
            name="text"
            value={input}
            id="todo"
            placeholder="Add a todo"
            className="todo-input"
            onChange={changeHandler}
            ref={inputRef}
          />
          <button className="todo-button" type="submit">
            Add Todo
          </button>
        </>
      )}
    </form>
  );
};

export default Form;
